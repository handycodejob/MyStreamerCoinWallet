
## v0.1.0 - SANCHEZ (2018-01-21)

### Added

 * Add gitlab CI to host static site generation
 * well-know for ssl from lets encrypt
 * StreamerCoinAbi
 * StreamerCoinTokens - the BLSK coin entry 

### Changed

 * Default node changed to our node on ved
 * Nodelist only has rinkeby, ved, and test nodes
 * dist and chrome-extension build files are no longer included
 * most warning are hidden
 * unused tabs are hidden
