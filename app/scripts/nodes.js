'use strict';
var nodes = function() {}
nodes.customNode = require('./nodeHelpers/customNode');
nodes.streamercoin = require('./nodeHelpers/streamercoin');
nodes.metamaskNode = require('./nodeHelpers/metamask');
nodes.nodeTypes = {
    Rinkeby: "RINKEBY ETH",
    Custom: "CUSTOM ETH",
    Testing: "TESTING",
    StreamerCoin: "STRM"
};
nodes.ensNodeTypes = [nodes.nodeTypes.ETH, nodes.nodeTypes.Ropsten];
nodes.domainsaleNodeTypes = [nodes.nodeTypes.ETH, nodes.nodeTypes.Ropsten];
nodes.customNodeObj = {
    'name': 'CUS',
    'blockExplorerTX': '',
    'blockExplorerAddr': '',
    'type': nodes.nodeTypes.Custom,
    'eip155': false,
    'chainId': '',
    'tokenList': [],
    'abiList': [],
    'service': 'Custom',
    'lib': null
};

nodes.nodeList = {
    'rin_ethscan': {
        'name': 'Rinkeby',
        'type': nodes.nodeTypes.Rinkeby,
        'blockExplorerTX': 'https://rinkeby.etherscan.io/tx/[[txHash]]',
        'blockExplorerAddr': 'https://rinkeby.etherscan.io/address/[[address]]',
        'eip155': true,
        'chainId': 4,
        'tokenList': require('./tokens/rinkebyTokens.json'),
        'abiList': require('./abiDefinitions/rinkebyAbi.json'),
        'service': 'Etherscan.io',
        'lib': require('./nodeHelpers/etherscanRin')
    },
    'sc_ethscan': {
        'name': 'StreamerCoin',
        'type': nodes.nodeTypes.StreamerCoin,
        'blockExplorerTX': 'https://explorer.streamerlounge.com:/#/tx/[[txHash]]',
        'blockExplorerAddr': 'https://explorer.streamerlounge.com/#/address/[[address]]',
        'eip155': false,
        'chainId': 4242,
        'tokenList': require('./tokens/streamerCoinTokens.json'),
        'abiList': require('./abiDefinitions/streamerCoinAbi.json'),
        'service': 'StreamerCoin',
        'lib': new nodes.streamercoin("https://rpc.streamerlounge.com")
    },
    'sc_ganache': {
        'name': 'Ganace',
        'type': nodes.nodeTypes.Custom,
        'blockExplorerTX': '',
        'blockExplorerAddr': '',
        'eip155': false,
        'chainId': 5777,
        'tokenList': require('./tokens/rinkebyTokens.json'),
        'abiList': require('./abiDefinitions/rinkebyAbi.json'),
        'service': 'Test',
        'lib': new nodes.streamercoin("http://127.0.0.1:7545")
    }
};


nodes.ethPrice = require('./nodeHelpers/ethPrice');
module.exports = nodes;
